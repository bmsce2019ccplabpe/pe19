#include<stdio.h>
int
main ()
{
  int a[20], n, key, i, beg, end, mid, flag = 0;
  printf ("Enter size of the array \n");
  scanf ("%d", &n);
  printf ("Enter array elements strictly in ascending order \n");
  for (i = 0; i < n; i++)
    scanf ("%d", &a[i]);
  printf ("Enter key value to be searched in array \n");
  scanf ("%d", &key);
  beg = 0;
  end = n - 1;
  while (beg <= end)
    {
      mid = (beg + end) / 2;
      if (a[mid] == key)
	{
	  flag = 1;
	  printf ("Element %d found at position %d \n", key, mid);
	  break;
	}
      else if (key > a[mid])
	{
	  beg = mid + 1;
	}
      else
	{
	  end = mid - 1;
	}
    }
  if (flag = 0)
    printf ("Element %d not found in the array \n", key);
  return 0;
}